#!/bin/bash
if [ ! -d "reveal.js" ]; then
    wget -qO - https://github.com/hakimel/reveal.js/archive/3.7.0.tar.gz | tar xfz - 
    mv reveal.js-3.7.0 reveal.js
fi

docker run --rm -v `pwd`:/app -w /app zenman94/pandoc pandoc -s -f markdown -t revealjs -o index.html slides.md -V revealjs-url=./reveal.js -V transition=concave -V theme=blood --include-in-header=css/perso.css
