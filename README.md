## Présentation GitLab - Kubernetes / Meetup CNCF Bordeaux #2

### Générateur de slide
Présentation générée avec Pandoc pour la conversion Markdown => Reveal.js

Une version locale des slides peut être générée avec `./generate-local.sh`

### CI
Présentation d'un CI de construction/test/preview et déploiement sur un cluster Kubernetes avec GitLab-CI

### Pages
Les slides de cette présentation sont disponible sur les [pages de ce projet](https://gaetan.ars.gitlab.io/gitlab-k8s/)
